# Consultas

## Mutation

    mutation {
        createStudent(input: {
            name: "Julian"
            email: "querol1993@gnail.com"
        }){
            _id,
            name,
            email
        }
    }

## query

    {
        getStudents{
            _id
            name
            email
        }
    }


    {
        getStudent(id: "5e4d79959d073830900cc2b8"){
            _id
            name
            email
        }
    }
