"use strict";

const connectDb = require("./db");
const { ObjectID } = require("mongodb");
const errorHandler = require("./errorHandler");

module.exports = {
  createTrayecto: async (root, { input }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newTrayecto = { ...input, fecha };
    let db;
    let trayectos;

    try {
      db = await connectDb();
      trayectos = await db.collection("trayectos").insertOne(newTrayecto);
      newTrayecto._id = trayectos.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newTrayecto;
  },
  createViaje: async (root, { idInicio, input }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newTrayecto = { ...input, fecha };
    try {
      let db = await connectDb();
      let trayectoVuelta = await db
        .collection("trayectos")
        .insertOne(newTrayecto);
      let inicio = await db
        .collection("trayectos")
        .findOne({ _id: ObjectID(idInicio) });
      const viaje = {
        idInicio: inicio._id,
        idFin: trayectoVuelta.insertedId,
        km: newTrayecto.odometro - inicio.odometro,
        fecha
      };

      const result = await db.collection("viajes").insertOne(viaje);
      viaje._id = result.insertedId;
      return viaje;
    } catch (error) {
      errorHandler(error);
    }
  },
  createRepostaje: async (root, { input }) => {
    const fecha = new Date().toLocaleString("es-ES");
    const newRepostaje = { ...input, fecha };
    let db;
    let repostaje;
    try {
      db = await connectDb();
      repostaje = await db.collection("repostajes").insertOne(newRepostaje);
      newRepostaje._id = repostaje.insertedId;
    } catch (error) {
      errorHandler(error);
    }
    return newRepostaje;
  }
};
