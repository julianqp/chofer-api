"use strict";

const connectDb = require("./db");
const { ObjectID } = require("mongodb");
const errorHandler = require("./errorHandler");

module.exports = {
  getTrayectos: async () => {
    let db;
    let trayectos = [];

    try {
      db = await connectDb();
      trayectos = await db
        .collection("trayectos")
        .find()
        .toArray();
    } catch (error) {
      errorHandler(error);
    }

    return trayectos;
  },
  getTayectoInicio: async () => {
    let db;
    let trayecto;
    try {
      db = await connectDb();
      trayecto = await db
        .collection("trayectos")
        .find({})
        .sort({ _id: -1 })
        .limit(1)
        .toArray();
    } catch (error) {
      errorHandler(error);
    }
    if (trayecto && trayecto[0] && trayecto[0].trayecto === "inicio")
      return { OK: true, trayecto: trayecto[0] };
    else return { OK: false, trayecto: null };
  },
  getViajes: async () => {
    let db;
    let viajes = [];
    try {
      db = await connectDb();
      viajes = await db
        .collection("viajes")
        .find()
        .toArray();
    } catch (error) {
      errorHandler(error);
    }
    return viajes;
  },
  getRepostajes: async () => {
    let db;
    let repostajes = [];
    try {
      db = await connectDb();
      repostajes = await db
        .collection("repostajes")
        .find()
        .toArray();
    } catch (error) {
      errorHandler(error);
    }
    return repostajes;
  }
};
